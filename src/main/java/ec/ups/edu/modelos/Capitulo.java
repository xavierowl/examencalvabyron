package ec.ups.edu.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Capitulo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cap_id;
	
	private String cap_nombre;
	
	private int cap_paginas;
	
	@OneToOne
	@JoinColumn(name="cap_autor")
	private Autor cap_autor;
	
	@ManyToOne
	@JoinColumn(name="cap_libro")
	private Libro cap_libro;

	public Autor getCap_autor() {
		return cap_autor;
	}

	public void setCap_autor(Autor cap_autor) {
		this.cap_autor = cap_autor;
	}

	public int getCap_id() {
		return cap_id;
	}

	public void setCap_id(int cap_id) {
		this.cap_id = cap_id;
	}

	public String getCap_nombre() {
		return cap_nombre;
	}

	public void setCap_nombre(String cap_nombre) {
		this.cap_nombre = cap_nombre;
	}

	public Libro getCap_libro() {
		return cap_libro;
	}

	public void setCap_libro(Libro cap_libro) {
		this.cap_libro = cap_libro;
	}

	public int getCap_paginas() {
		return cap_paginas;
	}

	public void setCap_paginas(int cap_paginas) {
		this.cap_paginas = cap_paginas;
	}

	@Override
	public String toString() {
		return "Capitulo [cap_id=" + cap_id + ", cap_nombre=" + cap_nombre + ", cap_paginas=" + cap_paginas
				+ ", cap_autor=" + cap_autor + ", cap_libro=" + cap_libro + "]";
	}
}
