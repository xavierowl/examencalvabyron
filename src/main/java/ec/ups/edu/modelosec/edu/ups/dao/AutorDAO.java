package ec.ups.edu.modelosec.edu.ups.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ec.ups.edu.modelos.Autor;
import ec.ups.edu.modelos.Libro;

//Los DAO's son beans stateless
@Stateless
public class AutorDAO {
	@PersistenceContext
	private EntityManager em;
			
	public List<Autor> listarAutores() throws Exception{
		try {
			return em.createQuery("SELECT a FROM Autor a", Autor.class).getResultList();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public Autor obtenerAutor(String codigo) throws Exception{
		try {
			return em.createQuery("SELECT a FROM Autor a WHERE a.aut_id = "+codigo, Autor.class).getSingleResult();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
