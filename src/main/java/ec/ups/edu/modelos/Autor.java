package ec.ups.edu.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Autor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int aut_id;
	
	private String aut_nombre;
	
	private String aut_apellido;
	
	public int getAut_id() {
		return aut_id;
	}
	public void setAut_id(int aut_id) {
		this.aut_id = aut_id;
	}
	public String getAut_nombre() {
		return aut_nombre;
	}
	public void setAut_nombre(String aut_nombre) {
		this.aut_nombre = aut_nombre;
	}
	public String getAut_apellido() {
		return aut_apellido;
	}
	public void setAut_apellido(String aut_apellido) {
		this.aut_apellido = aut_apellido;
	}
	@Override
	public String toString() {
		return aut_nombre + "(Código: " + aut_id+")";
	}
	
}

