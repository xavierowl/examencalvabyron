package ece.edu.ups.on;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;

import ec.ups.edu.modelos.Autor;
import ec.ups.edu.modelos.Libro;

@Local
public interface GestionLibrosLocal {
	public String crearLibro (Libro libro) throws Exception;
	public List<Autor> listarAutores () throws Exception;
	public Autor obtenerAutor(String codigo) throws Exception;
	public List<Libro> listarLibros() throws Exception;
}
