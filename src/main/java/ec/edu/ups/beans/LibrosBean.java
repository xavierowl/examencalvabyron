package ec.edu.ups.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import ec.ups.edu.modelos.Autor;
import ec.ups.edu.modelos.Capitulo;
import ec.ups.edu.modelos.Libro;
import ece.edu.ups.on.GestionLibrosLocal;

@ManagedBean
@SessionScoped
public class LibrosBean {
	@Inject
	private GestionLibrosLocal gestion;

	private String isbn;
	
	private String titulo;
	
	private String edicion;
	
	private String editorial;
	
	private String cap_nombre;
	
	private int paginas;
	
	private String mensaje;
	
	private List<Autor> autores;
	
	private String autor;
	
	private List<Capitulo> capitulos;
	
	private List<Libro> libros;

	@PostConstruct
	public void init() {
		try {
			autor = "";
			capitulos = new ArrayList<Capitulo>();
			autores = gestion.listarAutores();
			libros = gestion.listarLibros();
		} catch (Exception e) {
			mensaje = e.getMessage();
		}
	}
	
	public List<Libro> getLibros() {
		return libros;
	}

	public void setLibros(List<Libro> libros) {
		this.libros = libros;
	}

	public GestionLibrosLocal getGestion() {
		return gestion;
	}

	public void setGestion(GestionLibrosLocal gestion) {
		this.gestion = gestion;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEdicion() {
		return edicion;
	}

	public void setEdicion(String edicion) {
		this.edicion = edicion;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getCap_nombre() {
		return cap_nombre;
	}

	public void setCap_nombre(String cap_nombre) {
		this.cap_nombre = cap_nombre;
	}

	public int getCap_paginas() {
		return paginas;
	}

	public void setCap_paginas(int cap_paginas) {
		this.paginas = cap_paginas;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public List<Capitulo> getCapitulos() {
		return capitulos;
	}

	public void setCapitulos(List<Capitulo> capitulos) {
		this.capitulos = capitulos;
	}
	
	public String addLibro() {
		if(isbn.equals("")|| edicion.equals("")||editorial.equals("")||titulo.equals("")) {
			System.out.println("1");
			mensaje = "1 o más campos están vacíos";
			return null;
		}
		if(capitulos.size()==0) {
			mensaje = "No se ah añadido ningún capítulo";
			return null;
		}
		
		try {
			Libro libro = new Libro();
			libro.setLib_isbn(isbn);
			libro.setLib_edicion(edicion);
			libro.setLib_editorial(editorial);
			libro.setLib_capitulos(capitulos);
			libro.setLib_titulo(titulo);
			System.out.println(gestion.crearLibro(libro));
			return "Listar-Libros";
		} catch (Exception e) {
			mensaje = e.getMessage();
			return null;
		}
	}
	
	public void addCap() {
		Capitulo capitulo = new Capitulo();
		try {
			capitulo.setCap_autor(gestion.obtenerAutor(autor.split(" ")[1].split("\\)")[0]));
			mensaje = "Capítulo añadido con éxito";
		} catch (Exception e) {
			mensaje = e.getMessage();
		}
		capitulo.setCap_nombre(cap_nombre);
		capitulo.setCap_paginas(paginas);
		capitulos.add(capitulo);
	}
}
