package ece.edu.ups.on;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import ec.ups.edu.modelos.Autor;
import ec.ups.edu.modelos.Libro;

@Remote
public interface GestionLibrosRemote {
	public String crearLibro (Libro libro) throws Exception;
	public List<Autor> listarAutores () throws Exception;
	public Autor obtenerAutor(String codigo) throws Exception;
	public List<Libro> listarLibros() throws Exception;
}
