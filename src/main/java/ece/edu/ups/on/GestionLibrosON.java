package ece.edu.ups.on;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.ups.edu.modelos.Autor;
import ec.ups.edu.modelos.Libro;
import ec.ups.edu.modelosec.edu.ups.dao.AutorDAO;
import ec.ups.edu.modelosec.edu.ups.dao.LibroDAO;

@Stateless
public class GestionLibrosON implements GestionLibrosRemote, GestionLibrosLocal{
	@Inject
	private LibroDAO ldao;
	
	@Inject
	private AutorDAO adao;
	
	public String crearLibro(Libro libro) throws Exception{
		return ldao.crearLibro(libro);
	}
	
	public List<Autor> listarAutores() throws Exception{
		return adao.listarAutores();
	}
	
	public Autor obtenerAutor(String codigo) throws Exception{
		return adao.obtenerAutor(codigo);
	}
	
	public List<Libro> listarLibros() throws Exception{
		try {
			return ldao.listarLibros();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
