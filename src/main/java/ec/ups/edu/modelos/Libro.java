package ec.ups.edu.modelos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Libro {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int lib_id;
	
	private String lib_isbn;
	
	private String lib_titulo;
	
	private String lib_edicion;
	
	private String lib_editorial;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "cap_libro")
	private List<Capitulo> lib_capitulos;

	public int getLib_id() {
		return lib_id;
	}

	public void setLib_id(int lib_id) {
		this.lib_id = lib_id;
	}

	public String getLib_isbn() {
		return lib_isbn;
	}

	public void setLib_isbn(String lib_isbn) {
		this.lib_isbn = lib_isbn;
	}

	public String getLib_titulo() {
		return lib_titulo;
	}

	public void setLib_titulo(String lib_titulo) {
		this.lib_titulo = lib_titulo;
	}

	public String getLib_edicion() {
		return lib_edicion;
	}

	public void setLib_edicion(String lib_edicion) {
		this.lib_edicion = lib_edicion;
	}

	public String getLib_editorial() {
		return lib_editorial;
	}

	public void setLib_editorial(String lib_editorial) {
		this.lib_editorial = lib_editorial;
	}

	public List<Capitulo> getLib_capitulos() {
		return lib_capitulos;
	}

	public void setLib_capitulos(List<Capitulo> lib_capitulos) {
		this.lib_capitulos = lib_capitulos;
	}

	@Override
	public String toString() {
		return "Libro [lib_id=" + lib_id + ", lib_isbn=" + lib_isbn + ", lib_titulo=" + lib_titulo + ", lib_edicion="
				+ lib_edicion + ", lib_editorial=" + lib_editorial + ", lib_capitulos=" + lib_capitulos + "]";
	}
}
