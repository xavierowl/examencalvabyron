package ec.ups.edu.modelosec.edu.ups.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ec.ups.edu.modelos.Autor;
import ec.ups.edu.modelos.Libro;

//Los DAO's son beans stateless
@Stateless
public class LibroDAO {
	@PersistenceContext
	private EntityManager em;
			
	public String crearLibro(Libro libro) {
		try {
			em.persist(libro);
			return "El libro se ah registrado con éxito";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	public List<Libro> listarLibros() throws Exception{
		try {
			return em.createQuery("SELECT l FROM Libro l", Libro.class).getResultList();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
